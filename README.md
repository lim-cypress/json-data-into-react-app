# Getting JSON Data into React App with Jest and Cypress

This project was inspired by `Load and Render JSON Data into React Components` by Deeksha Sharma.
You can check the Pluralsight guide by this link.\
[https://www.pluralsight.com/guides/load-and-render-json-data-into-react-components]https://www.pluralsight.com/guides/load-and-render-json-data-into-react-components)

For my version I break up the code into different components, css and added some jest test.

## Available Scripts

In the project directory, you can run:

### `yarn run start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn test`

Launches the test runner.

### `yarn test:watch`

Launches the test runner in the interactive watch mode.

### `yarn test:coverage`

Launches the test runner with code coverage.

### `yarn lint`

Runs ESlint to check the `src` for linting issues.

### `yarn lint:fix`

Runs ESlint and try to fix the linting issues.

### `yarn cypress:open`

Runs Cypress Test with a Test Runner.

### `yarn cypress:run`

Runs Cypress Test in Headless Mode.

### `yarn cypress:ci`

Run Cypress Test with Start Server and Test. This is used to run for CI/CD pipeline.
