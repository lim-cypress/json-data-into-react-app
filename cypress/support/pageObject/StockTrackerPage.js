export default class StockTrackerPage {
    
    getRoot() {
        return cy.get('[data-testid=App]');
    }

    getHeader() {
        return cy.get('[data-testid=header]');
    }

    getTable() {
        return cy.get(':nth-child(1) > [data-testid=table] > tbody > tr > :nth-child(1)');
    }

    getCompany() {
        return cy.get(':nth-child(1) > [data-testid=table] > tbody > tr > :nth-child(1) > [data-testid=company]');
    }

    getTicker() {
        return cy.get(':nth-child(1) > [data-testid=table] > tbody > tr > :nth-child(2) > [data-testid=ticker]');
    }

    getStockPrice() {
        return cy.get(':nth-child(1) > [data-testid=table] > tbody > tr > :nth-child(3) > [data-testid=stockPrice]');
    }

    getTimeElapsed() {
        return cy.get(':nth-child(1) > [data-testid=table] > tbody > tr > :nth-child(4) > [data-testid=timeElapsed]')
    }
}

export const stockTrackerPage = new StockTrackerPage();