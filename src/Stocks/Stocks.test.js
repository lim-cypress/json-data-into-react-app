import { render, screen } from '@testing-library/react';
import Stocks from './Stocks';

beforeEach(() => {
  render(<Stocks />);
});

test('render Stocks', () => {
  expect(screen.getByTestId('stock-container')).toBeVisible();
});
