import React from 'react';
import './Stocks.css';
import { stockData } from '../db/data';
import HomePageHeader from '../Header/HomePageHeader';
import Table from '../Table/Table';

function Stocks() {
  return (
    <>
      <HomePageHeader />
      <div data-testid='stock-container' className='stock-container'>
        {stockData.map((data, key) => {
          return (
            <div key={key}>
              <Table
                key={key}
                company={data.company}
                ticker={data.ticker}
                stockPrice={data.stockPrice}
                timeElapsed={data.timeElapsed}
              />
            </div>
          );
        })}

      </div>
    </>
  )
};

export default Stocks;
