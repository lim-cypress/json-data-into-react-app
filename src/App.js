import './App.css';
import Stocks from './Stocks/Stocks';

function App() {
  return (
    <div data-testid='App' className="App">
      <Stocks />
    </div>
  );
}

export default App;
