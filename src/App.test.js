import { render, screen } from '@testing-library/react';
import App from './App';

beforeEach(() => {
  render(<App />);

});

test('renders App', () => {
  expect(screen.getByTestId('App')).toBeVisible();
});
